//
//  NetworkManager.swift
//  Bitcoin rate
//
//  Created by Zhanserik on 01.02.2019.
//  Copyright © 2019 Zhanserik. All rights reserved.
//

import Foundation


class NetworkManager {
    
    static let shared = NetworkManager()
    private init() {}
    
    var currency: Сurrency?
    
    func getData(currencyIn: CurrenceValue, completionHandler: @escaping (Сurrency)->()) {
        
        guard let url = URL(string: "https://api.coindesk.com/v1/bpi/currentprice/\(currencyIn).json") else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler:  { (data, response, error) in
            
            if let error = error {
                print("Error get data: ", error.localizedDescription)
                return
            }
            
            //Parsing
            if let data = data {
                do {
                    
                    guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else {
                        print("Error parse JSON")
                        return
                    }
                    
                    guard let jsonBpi = json["bpi"] as? [String : Any] else {
                        print("Error get bpi")
                        return
                    }
                    
                    guard let jsonBp = jsonBpi["\(currencyIn)"] as? [String : Any] else {
                        print("Error get currency")
                        return
                    }
                    
                    let cu = Сurrency(
                        code: jsonBp["code"] as! String,
                        description: jsonBp["description"] as! String,
                        rate: jsonBp["rate_float"] as! Float64)
                    
                    
                    self.currency = cu
                    
                    completionHandler(cu)
                    
                } catch let parsingError {
                    print("Parsing error: ", parsingError.localizedDescription)
                }
            }
        })
        task.resume()
    }
    
    func getHistory(currency: Сurrency, period: Periods) {
        
        var startDate = ""
        var endDate = ""
        
        switch period {
        case .Week:
            
            let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
            let startWeek = Date().getLast7Day()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            startDate = dateFormatter.string(from: startWeek!)
            endDate = dateFormatter.string(from: yesterday!)
            
            print("Week start: ", startDate)
            print("Week end: ", endDate)
            
        case .Month:
            
            let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
            let startMonth = Date().getLast30Day()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            startDate = dateFormatter.string(from: startMonth!)
            endDate = dateFormatter.string(from: yesterday!)
            
            print("Month start: ", startDate)
            print("Month end: ", endDate)
            
        case .Year:
            startDate = "2018-01-01"
            endDate = "2018-12-31"
        }
        
        guard let url = URL(string: "https://api.coindesk.com/v1/bpi/historical/close.json?currency=\(currency.code!)&start=\(startDate)&end=\(endDate)") else { return }
        
        print("url: ", url)
        
        let task = URLSession.shared.dataTask(with: url, completionHandler:  { (data, response, error) in
            
            if let error = error {
                print("Error get data: ", error.localizedDescription)
                return
            }
            
            //Parsing
            if let data = data {
                do {
                    
                    guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else {
                        print("Error parse JSON")
                        return
                    }
                    
                    guard let jsonBpi = json["bpi"] as? [String : Any] else {
                        print("Error get bpi")
                        return
                    }
                    
                    for i in jsonBpi {
                        
                        switch period {
                        case .Week:
                            
                           
                            print(i)
                        case .Month:
                            
                          print(i)
                            
                        case .Year:
                            print(i)
                        }
                    }
                    
                    
                    
//                    guard let jsonBp = jsonBpi["\(currencyIn)"] as? [String : Any] else {
//                        print("Error get currency")
//                        return
//                    }
                    
//                    let cu = Сurrency(
//                        code: jsonBp["code"] as! String,
//                        description: jsonBp["description"] as! String,
//                        rate: jsonBp["rate_float"] as! Float64)
//
//
//                    self.currency = cu
//
//                    completionHandler(cu)
                    
                    //                    print("self.currency", self.currency?.code as Any)
                    //                    print("self.currency", self.currency?.rate as Any)
                    //                    print("self.currency", self.currency?.description as Any)
                    
                } catch let parsingError {
                    print("Parsing error: ", parsingError.localizedDescription)
                }
            }
        })
        task.resume()
        
        
    }
    
    func addToArray() {
//        for i in 0..<3 {
//            
//        }
    }
}
