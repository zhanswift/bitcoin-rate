//
//  Model.swift
//  Bitcoin rate
//
//  Created by Zhanserik on 01.02.2019.
//  Copyright © 2019 Zhanserik. All rights reserved.
//

import Foundation

enum CurrenceValue {
    case KZT, USD, EUR
}

enum Periods {
    case Week, Month, Year
}

class Сurrency {
    
    var code: String?
    var description: String?
    var rate: Float64?
    var last7DayesPrice: [PriceDate]?
    var lastMonthPrice: [PriceDate]?
    var lastYearPrice: [PriceDate]?
    
    init(code: String, description: String, rate: Float64) {
        self.code = code
        self.description = description
        self.rate = rate
    }
}

class PriceDate {
    var date: String?
    var price: Float64?
}

class ExchangeСurrency {
    
    var firstСurrency: Сurrency?
    var secondСurrency: Сurrency?
    var date: Date?
    var rate: Float64?
    
    init(firstСurrency: Сurrency, secondСurrency: Сurrency, date: Date, rate: Float64) {
        self.firstСurrency = firstСurrency
        self.secondСurrency = secondСurrency
        self.date = date
        self.rate = rate
    }
}
