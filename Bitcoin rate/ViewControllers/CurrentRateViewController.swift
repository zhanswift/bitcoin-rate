//
//  CurrentRateViewController.swift
//  Bitcoin rate
//
//  Created by Zhanserik on 01.02.2019.
//  Copyright © 2019 Zhanserik. All rights reserved.
//

import UIKit
import Charts

class CurrentRateViewController: UIViewController, ChartViewDelegate {
    
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var periodSegmented: UISegmentedControl!
    
    fileprivate let weeks: [String] = ["Mo", "Tu", "We", "Thu", "Fri", "Su", "Sy"]
    fileprivate let months: [String] = ["1 week", "2 week", "3 week", "4 week"]
    fileprivate let years: [String] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        barChartView.delegate = self
        barChartView.xAxis.labelPosition = .bottom
        barChartView.drawGridBackgroundEnabled = false
        barChartView.noDataText = "You need to provide data for the chart."
        barChartView.legend.enabled = false
        
        let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 5.0]
        setChart(dataPoints: weeks, values: unitsSold)
        
        NetworkManager.shared.getData(currencyIn: .KZT) { (cu) in
            DispatchQueue.main.async {
                self.label.text = self.convertToString(value: cu.rate!) + " 〒"
            }
        }
    }
    
    @IBAction func changeCurrency(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            NetworkManager.shared.getData(currencyIn: .KZT) { (cu) in
                
                NetworkManager.shared.getHistory(currency: cu, period: .Week)
                
                DispatchQueue.main.async {
                    self.label.text = self.convertToString(value: cu.rate!) + " 〒"
                }
            }
        case 1:
            NetworkManager.shared.getData(currencyIn: .USD) { (cu) in
                
                NetworkManager.shared.getHistory(currency: cu, period: .Month)
                
                DispatchQueue.main.async {
                    self.label.text = "$ " + self.convertToString(value: cu.rate!)
                }
            }
        case 2:
            NetworkManager.shared.getData(currencyIn: .EUR) { (cu) in
                
                NetworkManager.shared.getHistory(currency: cu, period: .Year)
                
                DispatchQueue.main.async {
                    self.label.text = "€ " + self.convertToString(value: cu.rate!)
                }
            }
        default:
            break
        }
    }
    
    @IBAction func changePeriod(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 5.0]
            setChart(dataPoints: weeks, values: unitsSold)
        case 1:
            let unitsSold = [20.0, 4.0, 6.0, 3.0]
            setChart(dataPoints: months, values: unitsSold)
        case 2:
            let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]
            setChart(dataPoints: years, values: unitsSold)
        default:
            break
        }
    }
    
    @IBAction func test(_ sender: UIButton) {
        
        
        
    }
    
    
    func setChart(dataPoints: [String], values: [Double]) {
        barChartView.setBarChartData(xValues: dataPoints, yValues: values)
    }
    
    func convertToString(value: Float64) -> String {
        let cutted = String(format: "%.2f", value)
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = " "
        if let number = formatter.number(from: cutted), let result = formatter.string(from: number) {
            return result
        }
        return ""
    }
    
}
